var config = {};

config.live = process.env.NODE_ENV === 'production';

config.credentials = {
  username: 'foo',
  password: 'bar'
}

config.hub = config.live
             ? 'astutestreet.com:2931'
             : process.env.ASTUTE_HUB
             || 'localhost:2931'

config.api = config.live
             ? 'astutestreet.com:2930'
             : process.env.ASTUTE_API
             || 'localhost:2930'

config.constroller = config.live
                     ? 'astutestreet.com:8083'
                     : process.env.ASTUTE_CONTROLLER
                     || 'localhost:8083'

module.exports = config;