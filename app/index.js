var auth = require('./auth');
var socket = require('./socket');

// Authorize with the socket server
auth.login(function(err, json){
  var response = typeof json === 'string' && JSON.parse(json);
  if (err || (typeof response === 'object' && response.error)) {
    console.error('Unable to login', err || response.error);
  } else {
    // Logged in and received token
    socket.connect({ token: response.token });
  }
});
