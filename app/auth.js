var config = require('./config'),
    request = require('request');

var auth = {};

auth.login = function(callback){
  // POST request options
  var options = {
    url: 'http://' + config.api + '/login',
    form: config.credentials
  };

  // Make login request
  request.post(options, function(err, res, body){
    callback(err, body);
  });
};

module.exports = auth;
