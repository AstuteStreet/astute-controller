var io = require('socket.io-client');
var request = require('request');
var util = require('util');
var config = require('./config');

var socket = {};

socket.connect = function(options){
  socket.io = io.connect('http://' + config.hub, {
    query: 'token=' + options.token,
    reconnection: true,
    reconnectionDelay: 1000,
    reconnectionDelayMax : 5000,
    reconnectionAttempts: 0
  });

  socket.io.on('error', function(error){
    console.error('Socket error', error);
  })

  //socket.io.on('connect', function (socket) {
  //  socket
  //    .on('authenticated', function () {
  //      //do other things
  //    })
  //    .emit('authenticate', {token: jwt}); //send the jwt
  //});

  socket.io.on('connect', function () {
    console.log('connected!');
    socket.io.emit('ping', { ping: Date.now() });
  });

  socket.io.on('pong', function(data){
    console.log('Received pong', data);
  });

  socket.io.on('lights', function(data){
    console.log('lights', data);

    var url = 'http://' + config.controller + '/ZWaveAPI/Run/devices[%d].instances[0].%s.%s(%s)';
    var value = data.params.action === 'on' ? '255' : '0';
    var path = util.format(url, data.query.id, data.query.type, 'Set', value);
    //var path = util.format(url, data.query.id, data.query.type, 'Get', '');
    console.log('Requesting path: ' + path);
    request(path, function(err, res, body){
      console.log('Got response for: ', socket.io.id, err, body);
    });

  });
};

module.exports = socket;